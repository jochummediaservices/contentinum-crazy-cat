<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 13:18
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Contentinum;

/**
 * The configuration provider for the App module
 *
 * Class ConfigProvider
 * @package Contentinum
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return include CON_ROOT_PATH . DS . 'src' . DS . __NAMESPACE__ . '/config/contentinum.config.php';
    }
}