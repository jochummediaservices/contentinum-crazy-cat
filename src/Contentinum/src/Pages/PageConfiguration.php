<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 13:51
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Contentinum\Pages;


use Zend\Config\Config;

class PageConfiguration
{
    /**
     * Name cache factory
     * @var string
     */
    const CONTENTINUM_CACHE = 'contentinum_cache_struture';

    /**
     * Cache name
     * @var string
     */
    private $cacheKey = null;

    /**
     * Zend\Cache\Storage\StorageInterface
     * @var \Zend\Cache\Storage\StorageInterface
     */
    protected $cache;

    /**
     * @return string
     */
    public function getCacheKey(): string
    {
        return $this->cacheKey;
    }

    /**
     * @param string $cacheKey
     */
    public function setCacheKey(string $cacheKey)
    {
        $this->cacheKey = $cacheKey;
    }

    /**
     * @return \Zend\Cache\Storage\StorageInterface
     */
    public function getCache(): \Zend\Cache\Storage\StorageInterface
    {
        return $this->cache;
    }

    /**
     * @param \Zend\Cache\Storage\StorageInterface $cache
     */
    public function setCache(\Zend\Cache\Storage\StorageInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * PageConfiguration constructor.
     * @param $cache
     * @param string|null $key
     */
    public function __construct($cache, string $key = null)
    {
        $this->setCache($cache);
        $this->setCacheKey($key);
    }

    /**
     * @param string $file
     * @return mixed|Config
     */
    public function get( string $file)
    {
        $cache = $this->getCache();
        if (! ($result = $cache->getItem($this->getCacheKey()) )){
            $result = new Config(include $file);
            $cache->setItem($this->getCacheKey(), $result);
        }
        return $result;
    }
}