<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 13:21
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Contentinum\Factory\Service;


use Interop\Container\ContainerInterface;
use Zend\Config\Config;

class ContentinumFactory
{
    /**
     * Contentinum logger configuration key
     *
     * @var string
     */
    const CONTENTINUM_CFG_FILE = 'etc_cfg_files';

    /**
     * Name cache factory
     * @var string
     */
    const CONTENTINUM_CACHE = 'contentinum_cache_struture';

    /**
     * @param ContainerInterface $container
     * @return null|Config
     */
    public function __invoke( ContainerInterface $container )
    {
        $configuration = $container->get('config');
        $configuration = $configuration['contentinum_config']['etc_cfg_files'];
        if (isset($configuration[static::CONTENTINUM_CFG_FILE])){
            $pathToFile = $configuration[static::CONTENTINUM_CFG_FILE];
            $cache = $container->get(static::CONTENTINUM_CACHE);
            if (!($result = $cache->getItem(static::CONTENTINUM_CFG_FILE))){
                $result = new Config(include $pathToFile);
                $cache->setItem(static::CONTENTINUM_CFG_FILE, $result);
            }
            return $result;
        } else {
            return null;
        }

    }
}