<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 13:33
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Contentinum\Factory\Cache;


use Interop\Container\ContainerInterface;

/**
 * Class PublicContentFactory
 * @package Contentinum\Factory\Cache
 */
class PublicContentFactory
{
    /**
     * @param ContainerInterface $container
     * @return \Zend\Cache\Storage\StorageInterface
     */
    public function __invoke( ContainerInterface $container )
    {
        $cache = \Zend\Cache\StorageFactory::factory(array(
            'adapter' => array(
                'name' => 'filesystem',
                'ttl' => 3600,
                'options' => array(
                    'namespace' => 'content',
                    'cache_dir' => CON_ROOT_PATH . '/data/cache/frontend'
                )
            ),
            'plugins' => array(
                // Don't throw exceptions on cache errors
                'exception_handler' => array(
                    'throw_exceptions' => true
                ),
                'serializer'
            )
        ));
        return $cache;
    }

}