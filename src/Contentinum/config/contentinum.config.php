<?php
namespace Contentinum;
return [
    'dependencies' => [
        'invokables' => [

        ],
        'factories' => [
            'contentinum_cache_public' => 'Contentinum\Factory\Cache\PublicContentFactory',
            'contentinum_cache_struture' => 'Contentinum\Factory\Cache\StrutureContentFactory',
        ]
    ],
    'contentinum_config' => [

    ]
];