<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 13:39
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Mcwork\Factory\Service;

use Contentinum\Factory\Service\ContentinumFactory;

/**
 * Class ButtonsFactory
 * @package Mcwork\Factory\Service
 */
class ButtonsFactory extends ContentinumFactory
{
    /**
     * Cache key buttons configuration
     *
     * @var string
     */
    const CONTENTINUM_CFG_FILE = 'mcwork_elements_buttons';

    /**
     * Name cache factory
     *
     * @var string
     */
    const CONTENTINUM_CACHE = 'mcwork_cache_structures';
}