<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 13:45
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Mcwork\Factory\Service;

/**
 * Class TableeditFactory
 * @package Mcwork\Factory\Service
 */
class TableeditFactory
{
    /**
     * Cache key buttons configuration
     *
     * @var string
     */
    const CONTENTINUM_CFG_FILE = 'mcwork_elements_tableedit';

    /**
     * Name cache factory
     *
     * @var string
     */
    const CONTENTINUM_CACHE = 'mcwork_cache_structures';
}