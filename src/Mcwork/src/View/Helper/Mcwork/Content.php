<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 13:04
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Mcwork\View\Helper\Mcwork;


use Zend\View\Helper\AbstractHelper;

/**
 * Class Content
 * @package Mcwork\View\Helper\Mcwork
 */
class Content extends AbstractHelper
{
    public function __invoke($page, $content, $key)
    {
        if ( isset($content->$key) && strlen($content->$key) > 0 ){
            return $content->$key;
        } else {
            switch ($key){
                case 'headline':
                case 'subheadline':
                    return $page;
                    break;
                default:
                    return '';
            }
            return '';
        }
    }
}