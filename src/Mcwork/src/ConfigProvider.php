<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 12:50
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */

namespace Mcwork;

/**
 * The configuration provider for the App module
 *
 * Class ConfigProvider
 * @package Mcwork
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return CON_ROOT_PATH . DS . 'src/Mcwork/config/mcwork.config.php';
    }
}