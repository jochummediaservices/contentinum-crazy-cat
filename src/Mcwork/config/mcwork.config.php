<?php
namespace Mcwork;

return [
    'dependencies' => [
        'invokables' => [
        ],
        'factories' => [

            'mcwork_cache_structures' => 'Mcwork\Factory\Cache\StructureFactory',
            'mcwork_cache_data' => 'Mcwork\Factory\Cache\DataFactory',


            'mcwork_buttons' => 'Mcwork\Factory\Service\ButtonsFactory',
            'mcwork_tableedit' => 'Mcwork\Factory\Service\TableeditFactory',
            'mcwork_toolbar' => 'Mcwork\Factory\Service\ToolbarFactory',
        ]
    ],
    'contentinum_config' => [
        'etc_cfg_pages' => [
            'mcwork_pages' => CON_ROOT_PATH . '/src/Mcwork/etc/pages.php',
        ],

        'etc_cfg_files' => [
            'mcwork_navigation' => [1 => CON_ROOT_PATH . '/src/Mcwork/etc/navigation.php'],

            'mcwork_elements_toolbar' => CON_ROOT_PATH . '/src/Mcwork/etc/elements/toolbar.php',
            'mcwork_elements_buttons' => CON_ROOT_PATH . '/src/Mcwork/etc/elements/buttons.php',
            'mcwork_elements_tableedit' => CON_ROOT_PATH . '/src/Mcwork/etc/elements/tableedit.php',
        ],
    ],
    'templates' => [
        'paths' => [
            'mcworkapp'    => [ CON_ROOT_PATH . '/src/' . '/Mcwork/templates/app'],
            'mcworklayout' => [ CON_ROOT_PATH . '/src/' . '/Mcwork/templates/layout'],
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'mcworkContent' => 'Mcwork\View\Helper\Mcwork\Content',
        ]
    ]
];