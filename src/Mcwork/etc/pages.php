<?php

return [
    '_default' => [
        'splitQuery' => 2,
        'title' => 'contentinum backend',
        'resource' => 'index',
        'charset' => 'utf-8',
        'locale' => 'de_DE',
        'language' => 'de',
        'headStyle' => '[data-responsive-menu]ul{display: none;}@media screen and (min-width: 40em) {.title-bar {display:none;}}@media screen and (min-width: 64em) {body {overflow-y: scroll;}}',
        'timeZone' => 'Europa/Berlin',
        'metaViewport' => '<meta name="viewport" content="width=device-width, initial-scale=1" />',
        'layout' => 'admin',
        'app' => [
            'entitymanager' => 'doctrine.entitymanager.orm_default'
        ],
        'assets' => [
            'path' => '/src/Mcwork/assets',
            'web' => '/assets/app',
            'sets' => ['mccorecss','mccorejs'],
            'collections' => [
                'mccorecss' => [
                    'debug' => false,
                    'area' => 'styles',
                    'type' => 'styles',
                    'attr' => ['media' => 'all', 'rel' => 'stylesheet'],
                    'assets' => [
                        '/backend/css/font-awesome.css',
                        '/backend/css/opensansregular.css',
                        '/backend/css/foundation.app.css',
                        '/backend/css/grid/foundation.grid.full.css',
                        '/backend/css/foundation.typo.css',
                        '/backend/css/buttons/foundation.buttons.css',
                        '/backend/css/content/foundation.callout.css',
                        '/backend/css/menu/foundation.menu.css',
                        '/backend/css/mcworkapp.css',
                        '/backend/css/foundation.helpers.css',
                    ],
                    'includes' => [
                        '/data/usr/share/min/css/cssmin-v3.0.1.php'
                    ],
                    'filters' => [
                        'mincss' => 'Assetic\Filter\CssMinFilter'
                    ]
                ],
                'mcdashboardcss' => [
                    'debug' => false,
                    'area' => 'styles',
                    'type' => 'styles',
                    'attr' => ['media' => 'all', 'rel' => 'stylesheet'],
                    'assets' => [
                        '/backend/css/font-awesome.css',
                        '/backend/css/opensansregular.css',
                        '/backend/css/foundation.app.css',
                        '/backend/css/foundation.typo.css',
                        '/backend/css/grid/foundation.grid.full.css',
                        '/backend/css/buttons/foundation.buttons.css',
                        '/backend/css/content/foundation.callout.css',
                        '/backend/css/content/foundation.accordion.css',
                        '/backend/css/menu/foundation.menu.css',
                        '/backend/css/menu/foundation.menuicon.css',
                        '/backend/css/menu/foundation.drilldown.css',
                        '/backend/css/menu/foundation.dropdown.css',
                        '/backend/css/menu/foundation.topbar.css',
                        '/backend/css/content/foundation.titlebar.css',
                        '/backend/css/mcworkapp.css',
                        '/backend/css/foundation.helpers.css',
                    ],
                    'includes' => [
                        '/data/usr/share/min/css/cssmin-v3.0.1.php'
                    ],
                    'filters' => [
                        'mincss' => 'Assetic\Filter\CssMinFilter'
                    ]
                ],
                'mctablescss' => [
                    'debug' => false,
                    'area' => 'styles',
                    'type' => 'styles',
                    'attr' => ['media' => 'all', 'rel' => 'stylesheet'],
                    'assets' => [
                        '/backend/css/font-awesome.css',
                        '/backend/css/opensansregular.css',
                        '/backend/css/foundation.app.css',
                        '/backend/css/foundation.typo.css',
                        '/backend/css/grid/foundation.grid.full.css',
                        '/backend/css/buttons/foundation.buttons.css',
                        '/backend/css/content/foundation.callout.css',
                        '/backend/css/content/foundation.accordion.css',
                        '/backend/css/menu/foundation.menu.css',
                        '/backend/css/menu/foundation.menuicon.css',
                        '/backend/css/menu/foundation.drilldown.css',
                        '/backend/css/menu/foundation.dropdown.css',
                        '/backend/css/menu/foundation.topbar.css',
                        '/backend/css/form/foundation.forms.css',
                        '/backend/css/content/foundation.pagination.css',
                        '/backend/css/content/foundation.titlebar.css',
                        '/backend/css/content/foundation.table.css',
                        '/backend/css/datatables/dataTables.foundation.css',
                        '/backend/css/mcworkapp.css',
                        '/backend/css/foundation.helpers.css',
                    ],
                    'includes' => [
                        '/data/usr/share/min/css/cssmin-v3.0.1.php'
                    ],
                    'filters' => [
                        'mincss' => 'Assetic\Filter\CssMinFilter'
                    ]
                ],
                'mccorejs' => [
                    'debug' => false,
                    'area' => 'inline',
                    'type' => 'js',
                    'attr' => ['type' => 'text/javascript'],
                    'assets' => [
                        '/backend/js/vendor/jquery-3.2.1.js',
                        '/backend/js/vendor/what-input.js',
                        '/backend/js/vendor/translations.js',
                        '/backend/js/mcwork.core.js',
                        '/backend/js/utils/mcwork.logger.js',
                        '/backend/js/utils/mcwork.language.js',
                        '/backend/js/utils/mcwork.arraymerge.js',
                        '/backend/js/utils/mcwork.attributes.js',
                        '/backend/js/utils/mcwork.parameter.js',
                        '/backend/js/utils/mcwork.colors.js',
                        '/backend/js/utils/mcwork.icons.js',
                        '/backend/js/utils/mcwork.request.js',
                        '/backend/js/foundation/foundation.core.js',
                        '/backend/js/foundation/foundation.util.mediaQuery.js',
                        '/backend/js/foundation/foundation.util.touch.js',
                        '/backend/js/mcwork.app.js'
                    ]
                ],
                'mcdashboardjs' => [
                    'debug' => false,
                    'area' => 'inline',
                    'type' => 'js',
                    'attr' => ['type' => 'text/javascript'],
                    'assets' => [
                        '/backend/js/vendor/jquery-3.2.1.js',
                        '/backend/js/vendor/what-input.js',
                        '/backend/js/vendor/translations.js',
                        '/backend/js/mcwork.core.js',
                        '/backend/js/utils/mcwork.logger.js',
                        '/backend/js/utils/mcwork.language.js',
                        '/backend/js/utils/mcwork.arraymerge.js',
                        '/backend/js/utils/mcwork.attributes.js',
                        '/backend/js/utils/mcwork.parameter.js',
                        '/backend/js/utils/mcwork.colors.js',
                        '/backend/js/utils/mcwork.icons.js',
                        '/backend/js/utils/mcwork.request.js',
                        '/backend/js/foundation/foundation.core.js',
                        '/backend/js/foundation/foundation.util.mediaQuery.js',
                        '/backend/js/foundation/foundation.util.touch.js',
                        '/backend/js/foundation/foundation.util.keyboard.js',
                        '/backend/js/foundation/foundation.util.box.js',
                        '/backend/js/foundation/foundation.util.nest.js',
                        '/backend/js/foundation/foundation.util.triggers.js',
                        '/backend/js/foundation/foundation.accordionMenu.js',
                        '/backend/js/foundation/foundation.drilldown.js',
                        '/backend/js/foundation/foundation.dropdownMenu.js',
                        '/backend/js/foundation/foundation.responsiveMenu.js',
                        '/backend/js/foundation/foundation.responsiveToggle.js',
                        '/backend/js/mcwork.app.js'
                    ],

                ],
                'mctablesjs' => [
                    'debug' => false,
                    'area' => 'inline',
                    'type' => 'js',
                    'attr' => ['type' => 'text/javascript'],
                    'assets' => [
                        '/backend/js/vendor/jquery-3.2.1.js',
                        '/backend/js/vendor/what-input.js',
                        '/backend/js/vendor/translations.js',
                        '/backend/js/mcwork.core.js',
                        '/backend/js/utils/mcwork.logger.js',
                        '/backend/js/utils/mcwork.language.js',
                        '/backend/js/utils/mcwork.arraymerge.js',
                        '/backend/js/utils/mcwork.attributes.js',
                        '/backend/js/utils/mcwork.parameter.js',
                        '/backend/js/utils/mcwork.colors.js',
                        '/backend/js/utils/mcwork.icons.js',
                        '/backend/js/utils/mcwork.request.js',
                        '/backend/js/utils/mcwork.tables.js',
                        '/backend/js/foundation/foundation.core.js',
                        '/backend/js/foundation/foundation.util.mediaQuery.js',
                        '/backend/js/foundation/foundation.util.touch.js',
                        '/backend/js/foundation/foundation.util.keyboard.js',
                        '/backend/js/foundation/foundation.util.box.js',
                        '/backend/js/foundation/foundation.util.nest.js',
                        '/backend/js/foundation/foundation.util.triggers.js',
                        '/backend/js/foundation/foundation.accordionMenu.js',
                        '/backend/js/foundation/foundation.drilldown.js',
                        '/backend/js/foundation/foundation.dropdownMenu.js',
                        '/backend/js/foundation/foundation.responsiveMenu.js',
                        '/backend/js/foundation/foundation.responsiveToggle.js',
                        '/backend/js/vendor/datatables/jquery.dataTables.js',
                        '/backend/js/vendor/datatables/dataTables.foundation.js',
                        '/backend/js/mcwork.app.js'
                    ]
                ],
            ],
        ]
    ],
    '/mcwork/dashboard' => [
        'resource' => 'authorresource',
        'metaTitle' => 'Dashboard',
        'template' => 'content/dashboard/dashboard',
        'toolbar' => 0,
        'tableedit' => 0,
        'bodyId' => 'mcworkdashboard',
        'pageContent' => [
            'headline' => 'Dashboard',
            'content' => ''
        ],
        'app'=> [
            'controller' => 'Mcwork\Action\BackendAction',
            'worker' => 'Mcwork\Mapper\Dashboard',
            'entity' => 'Contentinum\Entity\WebContent',
        ],
        'assets' => [
            'sets' => ['mcdashboardcss','mcdashboardjs'],
        ],

    ],


    '/mcwork/fieldtypes' => [
        'resource' => 'publisherresource',
        'metaTitle' => 'Accounttypes',
        'template' => 'content/accounts/types',
        'toolbar' => 1,
        'tableedit' => 1,
        'pageContent' => [
            'headline' => 'Accounttypes',
            'content' => ''
        ],
        'app' => [
            'controller' => 'Mcwork\Action\BackendAction',
            'worker' => 'Mcwork\Mapper\FindAll',
            'entity' => 'Contentinum\Entity\FieldTypes'
        ],
        'assets' => [
            'sets' => ['mctablescss','mctablesjs'],
        ],
    ],
    /*
    '/mcwork/fieldtypes/add' => array(
        'splitQuery' => 3,
        'resource' => 'publisherresource',
        'metaTitle' => 'add_Accounttypes',
        'template' => 'forms/standard',
        'toolbar' => 1,
        'tableedit' => 1,
        'pageContent' => array(
            'headline' => 'add_Accounttypes',
            'content' => ''
        ),
        'app' => array(
            'controller' => 'Mcwork\Controller\AddFormController',
            'worker' => 'Mcwork\Model\Accounts\SaveTypes',
            'entity' => 'Contentinum\Entity\FieldTypes',
            'formdecorators' => 'mcwork_form_decorators',
            'form' => 'Mcwork\Form\AccountTypesForm',
            'formaction' => '/mcwork/fieldtypes/add',
            'settoroute' => '/mcwork/fieldtypes'
        ),
        'assets' => array(
            'sets' => array('mcworkformstyles','mcworkhead','mcworkforms'),
        ),
    ),

    '/mcwork/fieldtypes/edit' => array(
        'splitQuery' => 3,
        'resource' => 'publisherresource',
        'metaTitle' => 'edit_Accounttypes',
        'template' => 'forms/standard',
        'toolbar' => 1,
        'tableedit' => 1,
        'pageContent' => array(
            'headline' => 'edit_Accounttypes',
            'content' => ''
        ),
        'app' => array(
            'controller' => 'Mcwork\Controller\EditFormController',
            'worker' => 'Mcwork\Model\Accounts\SaveTypes',
            'entity' => 'Contentinum\Entity\FieldTypes',
            'formdecorators' => 'mcwork_form_decorators',
            'form' => 'Mcwork\Form\AccountTypesForm',
            'formaction' => '/mcwork/fieldtypes/edit',
            'settoroute' => '/mcwork/fieldtypes',
            'querykey' => 'category',
            'setexclude' => array(
                'field' => 'id'

            ),
        ),
        'assets' => array(
            'sets' => array('mcworkformstyles','mcworkhead','mcworkforms'),
        ),
    ),


    '/mcwork/fieldtypes/delete' => array(
        'resource' => 'publisherresource',
        'app' => array(
            'controller' => 'Mcwork\Controller\DeleteController',
            'worker' => 'Mcwork\Model\Delete\Entries',
            'entity' => 'Contentinum\Entity\FieldTypes',
            'querykey' => 'category',
            'hasEntries' => array(
                'groups' => array(
                    'tablename' => 'Contentinum\Entity\IndexGroups',
                    'column' => 'fieldTypes'
                ),
                'accounts' => array(
                    'tablename' => 'Contentinum\Entity\Accounts',
                    'column' => 'fieldtypes'
                )
            ),
            'settoroute' => '/mcwork/fieldtypes'
        )
    ), */

];