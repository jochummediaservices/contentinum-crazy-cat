<?php
return [
    'navigation' => [
        'default' => [
            [
                'label' => 'Dashboard',
                'uri' => '/mcwork/dashboard',
                'resource' => 'authorresource',
                'order' => 1
            ],
            [
                'label' => 'Files',
                'uri' => '#',
                'resource' => 'authorresource',
                'order' => 2,
                'listClass' => 'has-submenu',
                'subUlClass' => 'submenu menu vertical',
                'pages' => [
                    [
                        'label' => 'PublicMedias',
                        'uri' => '/mcwork/publicmedias',
                        'resource' => 'authorresource'
                    ],
                    [
                        'label' => 'NoPublicFiles',
                        'uri' => '/mcwork/filesdenied',
                        'resource' => 'authorresource'
                    ],
                    [
                        'label' => 'MediaGroup',
                        'uri' => '/mcwork/filegroups',
                        'resource' => 'authorresource',
                        'listClass' => 'has-submenu',
                        'subUlClass' => 'submenu menu vertical',
                        'pages' => [
                            [
                                'label' => 'Dateigruppen zusammenfassen',
                                'uri' => '/mcwork/combinefilegroups',
                                'resource' => 'authorresource'
                            ]
                        ]
                    ]
                ]
            ],
            [
                'label' => 'mcContent',
                'uri' => '#',
                'order' => 3,
                'resource' => 'authorresource',
                'listClass' => 'has-submenu',
                'subUlClass' => 'submenu menu vertical',
                'pages' => [
                    [
                        'label' => 'Pages',
                        'uri' => '/mcwork/pages',
                        'resource' => 'publisherresource',
                        'listClass' => 'has-submenu',
                        'subUlClass' => 'submenu menu vertical',
                        'pages' => [
                            
                            [
                                'label' => 'PageAttribute',
                                'uri' => '/mcwork/pageattribute',
                                'resource' => 'managerresource'
                            ],
                            [
                                'label' => 'Links',
                                'uri' => '/mcwork/links',
                                'resource' => 'managerresource'
                            ],
                            [
                                'label' => 'Headlinks',
                                'uri' => '/mcwork/pageheader',
                                'resource' => 'managerresource'
                            ]
                        ]
                    ],
                    [
                        'label' => 'Contribution',
                        'uri' => '/mcwork/contribution',
                        'resource' => 'authorresource',
                        'listClass' => 'has-submenu',
                        'subUlClass' => 'submenu menu vertical',
                        'pages' => [
                            [
                                'label' => 'contributionProperties',
                                'uri' => '/mcwork/pagecontent',
                                'resource' => 'publisherresource'
                            ]
                        ]
                    ],
                    [
                        'label' => 'News & Blogs',
                        'uri' => '/mcwork/article',
                        'resource' => 'authorresource',
                        'listClass' => 'has-submenu',
                        'subUlClass' => 'submenu menu vertical',
                        'pages' => [
                            [
                                'label' => 'News & Blog Categories',
                                'uri' => '/mcwork/blogcategory',
                                'resource' => 'adminresource'
                            ],
                            [
                                'label' => 'News & Blog Settings',
                                'uri' => '/mcwork/blogsettings',
                                'resource' => 'adminresource'
                            ],
                            [
                                'label' => 'Blog & News Groups',
                                'uri' => '/mcwork/bloggroup',
                                'resource' => 'adminresource'
                            ]
                        ]
                    ]
                ],
                [
                    'label' => 'Navigation',
                    'uri' => '/mcwork/navigation',
                    'resource' => 'managerresource'
                ],
                [
                    'label' => 'Forms',
                    'uri' => '/mcwork/form',
                    'resource' => 'publisherresource'
                ],
                [
                    'label' => 'Maps',
                    'uri' => '/mcwork/maps',
                    'resource' => 'publisherresource'
                ],
                [
                    'label' => 'Organisationen',
                    'uri' => '/mcwork/accounts', // directory
                    'resource' => 'publisherresource',
                    'listClass' => 'has-submenu',
                    'subUlClass' => 'submenu menu vertical',
                    'pages' => [
                        [
                            'label' => 'Organizationgroups',
                            'uri' => '/mcwork/accountgroups',
                            'resource' => 'publisherresource'
                        ],
                        [
                            'label' => 'Organisation Tags',
                            'uri' => '/mcwork/accountags',
                            'resource' => 'publisherresource'
                        ],
                        [
                            'label' => 'Organisation to Tags',
                            'uri' => '/mcwork/accounttagassign',
                            'resource' => 'publisherresource'
                        ],
                        [
                            'label' => 'Organisationen mit Kontakten',
                            'uri' => '/mcwork/accountlink',
                            'resource' => 'publisherresource'
                        ]
                    ] // end sub fieldtypes
                ],
                [
                    'label' => 'Contacts',
                    'uri' => '/mcwork/contacts',
                    'resource' => 'publisherresource',
                    
                    'listClass' => 'has-submenu',
                    'subUlClass' => 'submenu menu vertical',
                    'pages' => [
                        [
                            'label' => 'Kontakte gruppieren',
                            'uri' => '/mcwork/contactgroup',
                            'resource' => 'publisherresource'
                        ]
                    ]
                ]
            ],
            [
                'label' => 'Administration',
                'uri' => '#',
                'order' => 4,
                'resource' => 'authorresource',
                'listClass' => 'has-submenu',
                'subUlClass' => 'submenu menu vertical',
                'pages' => [
                    [
                        'label' => 'Logins',
                        'uri' => '#',
                        'resource' => 'publisherresource',
                        'listClass' => 'has-dropdown',
                        'subUlClass' => 'dropdown',
                        'pages' => [
                            [
                                'label' => 'Users',
                                'uri' => '/mcwork/users',
                                'resource' => 'publisherresource'
                            ],
                            [
                                'label' => 'Usergroups',
                                'uri' => '/mcwork/usrgrp',
                                'resource' => 'managerresource'
                            ],
                            [
                                'label' => 'User in groups',
                                'uri' => '/mcwork/usringrp',
                                'resource' => 'adminresource'
                            ]
                        ]
                    ],
                    [
                        'label' => 'Logs',
                        'uri' => '/mcwork/logs',
                        'resource' => 'adminresource'
                    ],
                    
                    [
                        'label' => 'Caches',
                        'uri' => '#',
                        'resource' => 'adminresource',
                        'listClass' => 'has-dropdown',
                        'subUlClass' => 'dropdown',
                        'pages' => [
                            [
                                'label' => 'Cache',
                                'uri' => '/mcwork/cache',
                                'resource' => 'adminresource'
                            ],
                            [
                                'label' => 'Cache Settings',
                                'uri' => '/mcwork/cachesettings',
                                'resource' => 'adminresource'
                            ]
                        ]
                    ],
                    
                    [
                        'label' => 'Preferences',
                        'uri' => '/mcwork/preferences',
                        'resource' => 'adminresource'
                    ],
                    [
                        'label' => 'Redirects',
                        'uri' => '/mcwork/redirects',
                        'resource' => 'adminresource'
                    ],
                    
                    [
                        'label' => 'Emailtemplates',
                        'uri' => '#',
                        'resource' => 'managerresource',
                        'listClass' => 'has-dropdown',
                        'subUlClass' => 'dropdown',
                        'pages' => [
                            [
                                'label' => 'Emailtextemplates',
                                'uri' => '/mcwork/emailtemplates',
                                'resource' => 'managerresource'
                            ],
                            
                            [
                                'label' => 'Emailsignatures',
                                'uri' => '/mcwork/emailsignatures',
                                'resource' => 'managerresource'
                            ]
                        ]
                    ],
                    
                    [
                        'label' => 'Layout',
                        'uri' => '#',
                        'resource' => 'adminresource',
                        'listClass' => 'has-dropdown',
                        'subUlClass' => 'dropdown',
                        'pages' => [
                            [
                                'label' => 'Assets',
                                'uri' => '/mcwork/assets',
                                'resource' => 'adminresource'
                            ],
                            [
                                'label' => 'Assets Collection',
                                'uri' => '/mcwork/assetcollectionfiles',
                                'resource' => 'adminresource'
                            ],
                            [
                                'label' => 'Asset cache',
                                'uri' => '/mcwork/assetcache',
                                'resource' => 'adminresource'
                            ],
                            [
                                'label' => 'Templates',
                                'uri' => '/mcwork/templates',
                                'resource' => 'adminresource'
                            ]
                        ]
                    ],
                    [
                        'label' => 'Accounttypes',
                        'uri' => '/mcwork/fieldtypes',
                        'resource' => 'adminresource'
                    ],
                    [
                        'label' => 'About',
                        'uri' => '/mcwork/version',
                        'resource' => 'authorresource'
                    ]
                ]
            ]
        ],
        [
            'label' => 'Contentinum_Apps',
            'uri' => '#',
            'order' => 5,
            'resource' => 'authorresource'
        ]
    ]

];