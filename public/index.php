<?php
/**
 * contentinum-crazy-cat
 *
 * Initial version by: michael.jochum
 * Initial version created on: 22.10.2017 08:55
 *
 * @copyright Copyright (c) jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 */


if (php_sapi_name() === 'cli-server'
    && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))
) {
    return false;
}

define('DS', DIRECTORY_SEPARATOR);

// Define root path
$parts = explode(DS, realpath(dirname(__FILE__) . '/..'));
define("CON_ROOT_PATH", implode(DS, $parts));
$parts = explode(DS, realpath(dirname(__FILE__)));
define("DOCUMENT_ROOT", implode(DS, $parts));